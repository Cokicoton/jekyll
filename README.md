# jekyll

## Web app de déploiement de sauvegarde Wordpress

![capture](jekyll_screenshot.png)

* Configurer l'application dans Config.php
* Les sauvegardes de répertoire doivent etre au format tar.gz
* Le dump de la base doit comprendre les CREATE TABLE IF NOT EXISTS
* La créatio et la suppression d'environnement (Arborescence + base) n'est pas encore géré
