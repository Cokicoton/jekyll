<?php

class Config {
  public $buPath = '[SAVES_PATH]'; // répertoire des sauvegardes
  public $evtPath = '[ENVIRONNEMENT_PATH]'; // path des environements

  public $dbEvt = array('preprod' => 'base_preprod', 'rct' => 'base_rct'); // nom de l'environnement => nom de la base
  public $srcEvt = array('preprod' => 'repertoire_preprod', 'rct' => 'repertoire_rct'); // nom de l'environnement => nom du répertoire
  public $dbUser = '[USER]';
  public $dbPassword = '[PASSWORD]';
  public $dbHost = '[HOST]';

  public $fichierLog = 'jekyll.log'; //si dans même répertoire
  public $os = 'windows'; //windows or linux

  public $distantUrl = '[DISTANT]'; // url du site sauvegardé
  public $localUrls = array('preprod' => 'preprod.url.local', 'rct' => 'rct.url.local'); // nom de l'environnement => url de l'environnement
}
?>

