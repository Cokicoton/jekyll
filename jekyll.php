<?php
include_once "Config.php";
$config = new Config();
$erreur = '';
$log = '';
if(isset($_POST['recharger'])){ //L'utilisateur souhaite un rechargement
  if($_POST['evt_reload_select'] != 'no' || $_POST['evt_reload_input'] != ''){ // L'utilisateur a sélectionné ou créé un environement
    $evt = $_POST['evt_reload_select'] != 'no' ? $_POST['evt_reload_select'] : $_POST['evt_reload_input'];
    if($_POST['site_sources_reload'] != 'no' || $_POST['img_doc_reload'] != 'no' || $_POST['db_reload'] != 'no'){ // l'utilisateur à sélectionner au moins une ressource à recharger
      if($_POST['site_sources_reload'] != 'no'){ //Rechargement de l'ensemble des fichiers
        //Suppression des fichiers actuels
        $command = 'rmdir ' . $config->evtPath . $config->srcEvt[$evt];
        exec($command);
        $command = 'md ' . $config->evtPath . $config->srcEvt[$evt];
        exec($command);
        //récuperer les répertoires contenant l'archive
        if(file_exists($config->buPath . 'save_complet_files/mois_courant/' . $_POST['site_sources_reload'])){
          $sauvegarde = $config->buPath . 'save_complet_files/mois_courant/' . $_POST['site_sources_reload'];
        } elseif(file_exists($config->buPath . 'save_complet_files/annee_courante/' . $_POST['site_sources_reload'])){
          $sauvegarde = $config->buPath . 'save_complet_files/annee_courante/' . $_POST['site_sources_reload'];
        } elseif(file_exists($config->buPath . 'save_complet_files/annees_passees/' . $_POST['site_sources_reload'])){
          $sauvegarde = $config->buPath . 'save_complet_files/annees_passees/' . $_POST['site_sources_reload'];
        }
        if($config->os == 'windows') {
 
          $command = '"C:\Program Files\7-Zip\7z.exe" e ' . $sauvegarde . ' -o"' . $config->evtPath . $config->srcEvt[$evt] . '\\"';
          exec($command);
          $command = '"C:\Program Files\7-Zip\7z.exe" x ' . $config->evtPath . $config->srcEvt[$evt] . '\\' . str_replace('.gz', '', $_POST['site_sources_reload']) .
 ' -o"' . $config->evtPath . $config->srcEvt[$evt] . '\\"';
          exec($command);
          $command = 'del ' . $config->evtPath . $config->srcEvt[$evt] . '\\' . str_replace('.gz', '', $_POST['site_sources_reload']);
          exec($command);
        } else {
          $command = "tar -xzf " . $sauvegarde . " -C " . $config->evtPath . $config->srcEvt[$evt] . '/';
          exec($command);
        }
        $log .= "Jekyll - Mise à jour des répertoires de l'environnemennt " . $evt . "\n\n";
        //Modification du wp-config.php
        $wpConfig = fopen($config->evtPath . $config->srcEvt[$evt] . '\\' . "wp-config.php", "r");
        $out = "";
        if($wpConfig) {
          while(!feof($wpConfig)){
            $ligne = fgets($wpConfig);
            if(count(explode("define ( 'DB_NAME'", $ligne)) > 1){
              $out .= "define ( 'DB_NAME', '" . $config->dbEvt[$evt] . "' );\n";            
            } elseif(count(explode("define('DB_NAME'", $ligne)) > 1){
              $out .= "define ( 'DB_NAME', '" . $config->dbEvt[$evt] . "' );\n";
            } elseif(count(explode("define ( 'DB_USER'", $ligne)) > 1){
              $out .= "define ( 'DB_USER', '" . $config->dbUser . "' );\n";
            } elseif(count(explode("define('DB_USER'", $ligne)) > 1){
              $out .= "define ( 'DB_USER', '" . $config->dbUser . "' );\n";
            } elseif(count(explode("define ( 'DB_PASSWORD'", $ligne)) > 1){
              $out .= "define ( 'DB_PASSWORD', '" . $config->dbPassword . "' );\n";
            } elseif(count(explode("define('DB_PASSWORD'", $ligne)) > 1){
              $out .= "define ( 'DB_PASSWORD', '" . $config->dbPassword . "' );\n";
            } elseif(count(explode("define ( 'DB_HOST'", $ligne)) > 1){
              $out .= "define ( 'DB_HOST', '" . $config->dbHost . "' );\n";
            } elseif(count(explode("define('DB_HOST'", $ligne)) > 1){
              $out .= "define ( 'DB_HOST', '" . $config->dbHost . "' );\n";
            } else {
              $out .= $ligne . "\n";
            }
          }
          fclose($wpConfig);
          rename($config->evtPath . $config->srcEvt[$evt] . '\\' . "wp-config.php", $config->evtPath . $config->srcEvt[$evt] . '\\' . "wp-config-prod.php");
          file_put_contents($config->evtPath . $config->srcEvt[$evt] . '\\' . "wp-config.php", $out);
        }
      }
      if($_POST['img_doc_reload'] != 'no'){ //Rechargement de répertoire Images et Documents
        //Suppression des fichiers actuels
        $command = 'rmdir ' . $config->evtPath . $config->srcEvt[$evt] . '\\Documents';
        exec($command);
        $command = 'rmdir ' . $config->evtPath . $config->srcEvt[$evt] . '\\Images';
        exec($command);
        $command = 'md ' . $config->evtPath . $config->srcEvt[$evt] . '\\Documents';
        exec($command);
        $command = 'md ' . $config->evtPath . $config->srcEvt[$evt] . '\\Images';
        exec($command);
        //récuperer les répertoires contenant l'archive
        if(file_exists($config->buPath . 'save_img_doc/mois_courant/' . $_POST['img_doc_reload'])){
          $sauvegarde = $config->buPath . 'save_img_doc/mois_courant/' . $_POST['img_doc_reload'];
        } elseif(file_exists($config->buPath . 'save_img_doc/annee_courante/' . $_POST['img_doc_reload'])){
          $sauvegarde = $config->buPath . 'save_img_doc/annee_courante/' . $_POST['img_doc_reload'];
        } elseif(file_exists($config->buPath . 'save_img_doc/annees_passees/' . $_POST['img_doc_reload'])){
          $sauvegarde = $config->buPath . 'save_img_doc/annees_passees/' . $_POST['img_doc_reload'];
        }
        if($config->os == 'windows') {
 
          $command = '"C:\Program Files\7-Zip\7z.exe" e ' . $sauvegarde . ' -o"' . $config->evtPath . $config->srcEvt[$evt] . '\\"';
          exec($command);
          $command = '"C:\Program Files\7-Zip\7z.exe" x ' . $config->evtPath . $config->srcEvt[$evt] . '\\' . str_replace('.gz', '', $_POST['img_doc_reload']) .
 ' -o"' . $config->evtPath . $config->srcEvt[$evt] . '\\"';
          exec($command);
          $command = 'del ' . $config->evtPath . $config->srcEvt[$evt] . '\\' . str_replace('.gz', '', $_POST['img_doc_reload']);
          exec($command);
        } else {
          $command = "tar -xzf " . $sauvegarde . " -C " . $config->evtPath . $config->srcEvt[$evt] . '/';
          exec($command);
        }
        $log .= "Jekyll - Mise à jour des répertoires Images/Documents " . $evt . "\n\n";
      }
      if($_POST['db_reload'] != 'no'){ //Rechargement de la base de donnée
  echo 'ouci';
        //récuperer les répertoires contenant l'archive
        if(file_exists($config->buPath . 'save_databases/mois_courant/' . $_POST['db_reload'])){
          $sauvegarde = $config->buPath . 'save_databases/mois_courant/' . $_POST['db_reload'];
        } elseif(file_exists($config->buPath . 'save_databases/annee_courante/' . $_POST['db_reload'])){
          $sauvegarde = $config->buPath . 'save_databases/annee_courante/' . $_POST['db_reload'];
        } elseif(file_exists($config->buPath . 'save_databases/annees_passees/' . $_POST['db_reload'])){
          $sauvegarde = $config->buPath . 'save_databases/annees_passees/' . $_POST['db_reload'];
        }

        if(isset($config->dbEvt[$evt])){
          $command = "mysql --user=" . $config->dbUser . " --password=" .$config->dbPassword . " " . $config->dbEvt[$evt] . " < " . $sauvegarde;
          exec($command);echo 'icici';
          $log .= "Jekyll - Mise à jour de la base de donnée de l'environnemennt " . $evt . ": --> " . $config->dbEvt[$evt] . "\n\n";
          $db = new PDO('mysql:host=' . $config->dbHost . ';dbname=' . $config->dbEvt[$evt] . ';charset=utf8', $config->dbUser, $config->dbPassword);
          $db->query("UPDATE wp_options SET option_value = replace(option_value, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db->query("UPDATE wp_posts SET post_content = replace(post_content, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db->query("UPDATE wp_postmeta SET meta_value = replace(meta_value, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db->query("UPDATE wp_usermeta SET meta_value = replace(meta_value, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db->query("UPDATE wp_links SET link_url = replace(link_url, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db->query("UPDATE wp_comments SET comment_content = replace(comment_content, '" . $config->distantUrl . "', '" . $config->localUrls[$evt] . "');");
          $db = null;
        } else {
          $erreur = "Veuillez sélectionner une base existante";
        }
        $log .= "Mise à jour base";
      }
    } else { // Aucune ressource sélectionnée
      $erreur = "Veuillez sélectionner une ou des ressource(s) à recharger";
    }

  } else { // Aucun environement sélectionné ou saisi
    $erreur = "Veuillez sélectionner ou saisir un environement";
  }
} elseif(isset($_POST['supprimer'])){ // L'utilisateur souhaite une suppression

  if($_POST['evt_reload_select'] != 'no') { // L'utilisateur a sélectionné
  } else { // Aucun environnement sélectionner
    $erreur = "Veuillez sélectionner un environement";
  }

file_put_contents('jekyll.log', $log);
file_put_contents('jekyll_error.log', $erreur);
}
?>
<DOCTYPE! html>
<html>
<head>
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action="jekyll.php" method="post">
  <fieldset>
    <legend>GMI - Recharger un environnement</legend>
    <label>Environnement</label>
    <select name="evt_reload_select" id="evt_reload_select">
    <?php
      $evts = $config->srcEvt;
      echo "<option value='no' selected></option>\n";
      foreach($evts as $nameEvt => $folder){echo $nameEvt;
        echo "<option value='" . $nameEvt . "'>" . $nameEvt . "</option>\n";
      }
    ?>
    </select>
    <input type="text" name="evt_reload_input" id="evt_reload_input" placeholder="Nouvel environnement"></input><br>
    <label>Fichiers site</label>
    <select name="site_sources_reload" id="site_sources_reload">
    <?php
      $siteSources =  scandir($config->buPath . "save_complet_files/mois_courant");
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_complet_files/annee_courante"));
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_complet_files/annees_passees"));
      echo "<option value='no' selected></option>\n";
      foreach($siteSources as $siteSource){
        echo $siteSource;
        if($siteSource != '.' && $siteSource != '..'){
          echo "<option value='" . $siteSource . "'>" . explode('_', $siteSource)[0] . " " . explode('_', $siteSource)[1] . "</option>\n";
        }
      }
    ?>
    </select><br>
    <label>Répertoire images/documents</label>
    <select name="img_doc_reload">
    <?php
      $siteSources =  scandir($config->buPath . "save_img_doc/mois_courant");
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_img_doc/annee_courante"));
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_img_doc/annees_passees"));
      echo "<option value='no' selected></option>\n";
      foreach($siteSources as $siteSource){
        echo $siteSource;
        if($siteSource != '.' && $siteSource != '..'){
          echo "<option value='" . $siteSource . "'>" . explode('_', $siteSource)[0] . " " . explode('_', $siteSource)[1] . "</option>\n";
        }
      }
    ?>
    </select><br>
    <label>Base de données</label>
    <select name="db_reload">
    <?php
      $siteSources =  scandir($config->buPath . "save_databases/mois_courant");
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_databases/annee_courante"));
      $siteSources = array_merge($siteSources, scandir($config->buPath . "save_databases/annees_passees"));
      echo "<option value='no' selected></option>\n";
      foreach($siteSources as $siteSource){
        echo $siteSource;
        if($siteSource != '.' && $siteSource != '..'){
          echo "<option value='" . $siteSource . "'>" . explode('_', $siteSource)[0] . " " . explode('_', $siteSource)[1] . "</option>\n";
        }
      }
    ?>
    </select><br>
    <label>Base locale spécifique</label>
    <input type="text" name="db_local" id="db_local" placeholder="Nouvelle base"></input><br>
    <input type="submit" value="Recharger" class="valider" name="recharger">
  </fieldset>
  <fieldset>
    <legend>GMI - Supprimer un environement</legend>
    <label>Environnement</label>
    <select name="evt_delete">
    <?php
      $evts =  $config->srcEvt;
      foreach($evts as $nameEvt => $folder){
        echo "<option value='" . $nameEvt . "'>" . $nameEvt . "</option>\n";
      }
    ?>
    </select><br>
    <input type="submit" value="Supprimer" class="valider" name="supprimer">
  </fieldset>
</form>
</body>
</html>
